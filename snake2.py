import sys, random
from PySide import QtCore, QtGui
#from snake import Snake,SnakePart
class Communicate(QtCore.QObject):
    
    msgToSB = QtCore.Signal(str)
    
class GUI(QtGui.QMainWindow):
    width,height,scale = 30,30,10
    def __init__(self):
        super(GUI, self).__init__()
        self.statusbar = self.statusBar()
        self.setGeometry(500, 501, BoardQt.width*BoardQt.scale, BoardQt.height*BoardQt.scale + self.statusbar.size().height())
        self.setWindowTitle('Python Snake')
        
        self.gameboard = BoardQt(self)

        self.setCentralWidget(self.gameboard)

        
        self.gameboard.c.msgToSB[str].connect(self.statusbar.showMessage)
            
        self.gameboard.start()
        self.center()

    def center(self):
        
        screen = QtGui.QDesktopWidget().screenGeometry()
        size =  self.geometry()
        self.move((screen.width()-size.width())/2, 
            (screen.height()-size.height())/2)
             
class BoardQt(QtGui.QFrame):
    width,height,scale,speed = 30,30,10,250
    def __init__(self,parent):
        super(BoardQt, self).__init__()
        self.snake = Snake(5,5)
        for s in range(5):
            self.snake.addPart(10-s,10)
        self.direction="right"
        self.nextDirection="right"
        self.score = 0
        self.food = []
        self.eaten = []
        
        self.isStarted = False
        self.isPaused = False
        self.timer = QtCore.QBasicTimer()
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        ##self.clearBoard()***TODO
        self.c = Communicate()
        
    def start(self):
        if self.isPaused:
            return

        self.isStarted = True

        self.c.msgToSB.emit(str(self.score))

        
        self.timer.start(BoardQt.speed, self)

    def pause(self):
        
        if not self.isStarted:
            return

        self.isPaused = not self.isPaused
        
        if self.isPaused:
            self.timer.stop()
            self.c.msgToSB.emit("paused")
        else:
            self.timer.start(BoardQt.speed, self)
            self.c.msgToSB.emit(str(self.score))

        self.update()
    def paintEvent(self, event):
        
        painter = QtGui.QPainter(self)
        self.drawGUI(painter)
        
        
    def drawGUI(self,painter): 
               
        x = BoardQt.width
        y = BoardQt.height
        scale = BoardQt.scale
        color = "blue"
        #head_color = "red"
        ###Board limits ###
        painter.setPen(QtGui.QColor("black"))
        painter.drawLine(0, 0, BoardQt.width*BoardQt.scale-1, 0)
        painter.drawLine(0, 0, 0, BoardQt.height*BoardQt.scale-1)
        painter.drawLine(BoardQt.width*BoardQt.scale-1, BoardQt.height*BoardQt.scale-1, BoardQt.width*BoardQt.scale-1, 0)
        painter.drawLine(BoardQt.width*BoardQt.scale-1, BoardQt.height*BoardQt.scale-1, 0, BoardQt.height*BoardQt.scale-1)
        
        ### TAIL ###
        #painter.setPen(QtGui.QColor(color).lighter())
        painter.setBrush(QtGui.QColor(0,100,255))
        for i in range(1,len(self.snake.snake)):
            p = self.snake.snake[i]
            #painter.setBrush(QtGui.QColor(color))
            painter.drawEllipse(p.x*scale,p.y*scale,scale,scale)
        p = self.snake.snake[0]
            #painter.setBrush(QtGui.QColor(color))
            
        painter.setPen(QtGui.QColor("red"))
        for i in range(len(self.eaten)):
            f = self.eaten[i]
            painter.drawEllipse(f.x*scale,f.y*scale,scale,scale) 
        painter.setPen(QtGui.QColor("black"))
        ### HEAD ###
        painter.setBrush(QtGui.QColor(color))
        a = 0
        alen = 300*16
        e = 30
        n = 15
        if self.direction=="up":
            if self.snake.eating:
                a = (90+e)*16
            else:
                a = (90+n)*16
                alen = 340*16
        elif self.direction=="down":
            if self.snake.eating:
                a = (-90+e)*16
            else:
                a = (-90+n)*16
                alen = 340*16
        elif self.direction=="left":
            if self.snake.eating:
                a = (180+e)*16
            else:
                a = (180+n)*16
                alen = 340*16
        else:
            if self.snake.eating:
                a = (0+e)*16
            else:
                a = (0+n)*16
                alen = 340*16
            
        painter.drawPie(p.x*scale,p.y*scale,scale,scale,a,alen)
        ### FOOD ###
        painter.setBrush(QtGui.QColor("orange"))
        for i in range(len(self.food)):
            f = self.food[i]
            painter.drawEllipse(f.x*scale,f.y*scale,scale,scale)
    def keyPressEvent(self, event):
        
        if not self.isStarted:
            QtGui.QWidget.keyPressEvent(self, event)
            return

        key = event.key()
        
        if key == QtCore.Qt.Key_P:
            self.pause()
            return
        if self.isPaused:
            return
        elif key == QtCore.Qt.Key_Left:
            self.nextDirection = "left"
            
        elif key == QtCore.Qt.Key_Right:
            self.nextDirection = "right"
            
        elif key == QtCore.Qt.Key_Down:
            self.nextDirection = "down"
            
        elif key == QtCore.Qt.Key_Up:
            self.nextDirection = "up"
            
        else:
            QtGui.QWidget.keyPressEvent(self, event)
            
    
            
    def isColliding(self):
        colliding = False
        x = self.snake.snake[0].x
        y = self.snake.snake[0].y
        for i in range(1,len(self.snake.snake)):
            if self.snake.snake[i].x == x and self.snake.snake[i].y == y:
                colliding = True
                break
        
        return colliding   
    
    def eat(self):
        #colliding = False
        x = self.snake.snake[0].x
        y = self.snake.snake[0].y
        x_last = self.snake.snake[-1].x
        y_last = self.snake.snake[-1].y
        for i in range(len(self.food)):
            if self.food[i].x == x and self.food[i].y == y:
                self.eaten.append(self.food[i])
                self.food.pop(i)
#                break
        print "length" + str(len(self.eaten))
        for i in range(len(self.eaten)):
            print i,
            if self.eaten[i].x == x_last and self.eaten[i].y == y_last:
                self.snake.addPart(x_last, y_last)
                e = self.eaten.pop(i)
                self.score +=e.value
                self.c.msgToSB.emit(str(self.score))
           
     
    def move(self):
        
        x = self.snake.snake[0].x
        y = self.snake.snake[0].y
        if self.direction=="up":
            if self.nextDirection != "down":
                self.direction = self.nextDirection
            y = self.snake.snake[0].y-1
        elif self.direction=="down":
            if self.nextDirection != "up":
                self.direction = self.nextDirection
            y = self.snake.snake[0].y+1
        elif self.direction=="left":
            if self.nextDirection != "right":
                self.direction = self.nextDirection
            x = self.snake.snake[0].x-1
        else:
            if self.nextDirection != "left":
                self.direction = self.nextDirection
            x = self.snake.snake[0].x+1
            
        if x>=BoardQt.width:
            x = 0
        elif x<0:
            x = BoardQt.width-1
            
        if y>=BoardQt.height:
            y = 0
        elif y<0:
            y = BoardQt.height-1
        l = len(self.snake.snake)
        for i in range(l-1,0,-1):
            self.snake.snake[i].x = self.snake.snake[i-1].x
            self.snake.snake[i].y = self.snake.snake[i-1].y
            
        self.snake.snake[0].x = x
        self.snake.snake[0].y = y
            
    def timerEvent(self, event):
        
        if event.timerId() == self.timer.timerId():
            self.move()
            if len(self.food)<1:
                self.addRandomFood()
            self.eat()
            self.eatAnim()
            self.update()
        else:
            QtGui.QFrame.timerEvent(self, event)
            
    def isFoodValid(self,x,y):
        valid = True
        if BoardQt.width > x and x >= 0:
            if BoardQt.height > y and y >= 0: 
                        for i in range(len(self.snake.snake)):
                            if self.snake.snake[i].x == x and self.snake.snake[i].y == y:
                                valid = False
                                break
            else:
                valid = False
        else:
            valid = False
        return valid
    
    def addRandomFood(self,value=1,expire=60):
        while True:
            x = random.randint(0,BoardQt.width)
            y = random.randint(0,BoardQt.height)
            if self.isFoodValid(x, y):
                food = Food(x, y, value, expire)
                self.food.append(food)
                break
            
    def eatAnim(self):
        x = self.snake.snake[0].x
        y = self.snake.snake[0].y
        if self.direction=="up":
            y = self.snake.snake[0].y-1
        elif self.direction=="down":
            y = self.snake.snake[0].y+1
        elif self.direction=="left":
            x = self.snake.snake[0].x-1
        else:
            x = self.snake.snake[0].x+1
        self.snake.eating = False
        for i in range(0,len(self.food)):
            if self.food[i].x==x and self.food[i].y==y:
                self.snake.eating = True
                break
            
class Snake:
    def __init__(self,x,y):
        self.snake = []
        self.eating = False
        #self.addPart(x, y)

    def addPart(self,x,y):
        self.snake.append(SnakePart(x,y))

class SnakePart:
    def __init__(self,x,y):
       self.x,self.y = x,y
       #self.follow = follow
    def __repr__(self):
        return str(self.x) + " " + str(self.y)
    
class Food:
    def __init__(self,x,y,value=1,expire=60):
        self.x = x
        self.y = y
        self.value = value
        self.expire = expire

def main():
    
    app = QtGui.QApplication(sys.argv)
    s = GUI()
    s.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()